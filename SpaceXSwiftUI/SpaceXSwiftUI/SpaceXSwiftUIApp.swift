//
//  SpaceXSwiftUIApp.swift
//  SpaceXSwiftUI
//
//  Created by José Letona Rodríguez on 11/3/21.
//

import SwiftUI

@main
struct SpaceXSwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
